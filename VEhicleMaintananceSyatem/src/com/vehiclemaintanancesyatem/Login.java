package com.vehiclemaintanancesyatem;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;

import android.content.*;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.ObbInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.*;
import com.loopj.android.http.*;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.view.View;
import android.view.View.OnClickListener;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class Login extends Activity {
    private static final String TAG = "";
    @SuppressLint("NewApi")
	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
			.permitAll().build();

	EditText edtusername, edtpassword;
	Button btnLogin, btnfp, btnsignup,btnforgetPassword;
	String username, password;
	String result = null;
	String line = null;
	int code;
	InputStream is = null;
	ProgressDialog dialog = null;
	Bundle b;
	HttpPost httppost;
	StringBuffer buffer;
	HttpResponse response;
	HttpClient httpclient;
	List<NameValuePair> nameValuePairs;
    ProgressDialog progress;
    Context context;
    CheckBox checkBox;
    SharedPreferences.Editor editor;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

        context = this;
        editor = getSharedPreferences("preferences", MODE_PRIVATE).edit();
		edtusername = (EditText) findViewById(R.id.edtusername);
		edtpassword = (EditText) findViewById(R.id.edtpassword);
        checkBox = (CheckBox) findViewById(R.id.checkBox);

        SharedPreferences prefs = getSharedPreferences("preferences", MODE_PRIVATE);
        String username1 = prefs.getString("username", "0");
        String password1 = prefs.getString("password", "0");

        if(!username1.equals("0")){
            edtusername.setText(username1);
            edtpassword.setText(password1);
        }else {
            edtusername.setText("");
            edtpassword.setText("");
        }

		btnLogin = (Button) findViewById(R.id.btnLogin);
		btnfp = (Button) findViewById(R.id.btnfp);
		btnsignup = (Button) findViewById(R.id.btnsp);
        btnforgetPassword = (Button) findViewById(R.id.btnfp);

		btnsignup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i = new Intent(Login.this, UserRegistration.class);
				startActivity(i);

			}
		});
        btnforgetPassword.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent i = new Intent(Login.this, ForgetPassword.class);
                startActivity(i);

            }
        });
        btnLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                username = edtusername.getText().toString();
                password = edtpassword.getText().toString();
                if(username.isEmpty()){
                    showAlert("Validation Error", "Username is empty");
                }else if(password.isEmpty()){
                    showAlert("Validation Error", "Password is empty");
                }else {
                    progress = ProgressDialog.show(context, "Please Wait","Loading...", true, true);
                    login();
                }
            }
        });

	}

	private void login() {
        AsyncHttpClient client = new AsyncHttpClient(){
            @Override
            public void setTimeout(int value) {
                super.setTimeout(60 * 1000);
            }
        };
        String url = "http://earningindia.com/VMMS/check.php";

        RequestParams params = new RequestParams();
        params.put("username", username);
        params.put("password", password);
        System.out.println(params.toString());
        client.post(url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                progress.dismiss();
                System.out.println("response = "+response);
                final String tempRes = response.toString();
                try{
                    if (!tempRes.equalsIgnoreCase("No Such User Found")) {
                        JSONObject obj = (JSONObject)response.get(0);
                        String msg = "Login Success, Welcome "+obj.getString("fullname");
                        Toast.makeText(getApplicationContext(), (String)msg,
                                Toast.LENGTH_LONG).show();
                        editor.putString("id", obj.getString("id"));
                        editor.putString("address", obj.getString("address"));
                        editor.putString("contactno", obj.getString("contactno"));
                        editor.putString("fullname", obj.getString("fullname"));
                        if(checkBox.isChecked()){
                            editor.putString("username", username);
                            editor.putString("password", password);
                        }else {
                            editor.remove("username");
                            editor.remove("password");
                        }

                        editor.commit();
                        Intent i = new Intent(Login.this, Home.class);
                        startActivity(i);
                    }
                }catch (JSONException e){
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progress.dismiss();
                showAlert("Error", "Login Failed. Invalid username or password.");
            }
        });
	}

	public void showAlert(final String title, final String message) {
		Login.this.runOnUiThread(new Runnable() {
			public void run() {
				AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
				builder.setTitle(title);
                builder.setCancelable(true);
				builder.setMessage(message);
                builder.setPositiveButton("Thank You",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                    }
                });
				AlertDialog alert = builder.create();
				alert.show();
			}
		});
	}


}
