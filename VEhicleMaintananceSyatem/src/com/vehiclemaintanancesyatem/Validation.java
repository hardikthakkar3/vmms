package com.vehiclemaintanancesyatem;

import android.widget.EditText;
import java.util.regex.Pattern;

public class Validation {

	// Regular Expression
	// you can change the expression based on your need
	private static final String PHONE_REGEX = "^[0-9]{10}$";
	private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,8})";

	// Error Messages
	private static final String REQUIRED_MSG = "required";
	private static final String PHONE_MSG = "##########";
	private static final String PASS_MSG = "Enter Password Between 6-8 characters";

	// call this method when you need to check email validation

	// call this method when you need to check phone number validation
	public static boolean isPhoneNumber(EditText editText, boolean required) {
		return isValid(editText, PHONE_REGEX, PHONE_MSG, required);
	}

	public static boolean isPassword(EditText editText, boolean required) {
		return isValid(editText, PASSWORD_PATTERN, PASS_MSG, required);
	}

	// return true if the input field is valid, based on the parameter passed
	public static boolean isValid(EditText editText, String regex,
			String errMsg, boolean required) {

		String text = editText.getText().toString().trim();
		// clearing the error, if it was previously set by some other values
		editText.setError(null);

		// text required and editText is blank, so return false
		if (required && !hasText(editText))
			return false;

		// pattern doesn't match so returning false
		if (required && !Pattern.matches(regex, text)) {
			editText.setError(errMsg);
			return false;
		}
		;

		return true;
	}

	// check the input field has any text or not
	// return true if it contains text otherwise false
	public static boolean hasText(EditText editText) {

		String text = editText.getText().toString().trim();
		editText.setError(null);

		// length 0 means there is no text
		if (text.length() == 0) {
			editText.setError(REQUIRED_MSG);
			return false;
		}

		return true;
	}

}
