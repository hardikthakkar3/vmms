package com.vehiclemaintanancesyatem;

public class Contacts {

	String fullnm;
	String addres;
	String vcn;
	String connum;
	String username;
	String password;
	String seq;
	String seqans;

	public Contacts(String fullname, String address, String vcno, String conno,
			String uname, String pass, String sq, String sqans) {
		// TODO Auto-generated constructor stu

		this.fullnm = fullname;
		this.addres = address;
		this.vcn = vcno;
		this.connum = conno;
		this.username = uname;
		this.password = pass;
		this.seq = sq;
		this.seqans = sqans;

	}

	public String getName() {
		return this.fullnm;
	}

	public void setName(String fullname) {
		this.fullnm = fullname;
	}

	public String getAddress() {
		return this.addres;
	}

	public void setAddress(String address) {
		this.addres = address;
	}

	public String getVcno() {
		return this.vcn;
	}

	public void setvcno(String vcno) {
		this.vcn = vcno;
	}

	public String getConno() {
		return this.connum;
	}

	public void setConno(String conno) {
		this.connum = conno;
	}

	public String getUname() {
		return this.username;
	}

	public void setUname(String uname) {
		this.username = uname;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String pass) {
		this.password = pass;
	}

	public String getSeque() {
		return this.seq;
	}

	public void setSeque(String sq) {
		this.addres = sq;
	}

	public String getAnswer() {
		return this.seqans;
	}

	public void setAnswer(String sqans) {
		this.seqans = sqans;
	}

}
