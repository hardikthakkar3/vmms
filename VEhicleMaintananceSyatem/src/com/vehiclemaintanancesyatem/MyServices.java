package com.vehiclemaintanancesyatem;
 
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.http.Header;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyServices extends Activity {
    ProgressDialog progress;
    Context context;
    //LIST OF ARRAY STRINGS WHICH WILL SERVE AS LIST ITEMS
    ArrayList<String> listItems;

    //DEFINING A STRING ADAPTER WHICH WILL HANDLE THE DATA OF THE LISTVIEW
    ArrayAdapter adapter;
    JSONArray jsonArray;
    @Override
    protected void onResume() {
        super.onResume();

        context = this;
        progress = ProgressDialog.show(context, "Please Wait","Loading...", true, true);
        listItems=new ArrayList<String>();


        adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_2,listItems){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                TwoLineListItem row;
                if(convertView == null){
                    LayoutInflater inflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    row = (TwoLineListItem)inflater.inflate(android.R.layout.simple_list_item_2, null);
                }else{
                    row = (TwoLineListItem)convertView;
                }
                row.setBackgroundColor(Color.BLACK);
                try {
                    row.getText1().setText(((JSONObject)jsonArray.get(position)).getString("serviceName")+"("+((JSONObject)jsonArray.get(position)).getString("serviceType")+")");
                    row.getText2().setText("Parts : "+((JSONObject)jsonArray.get(position)).getString("replaceParts")
                            +", Problem : "+((JSONObject)jsonArray.get(position)).getString("problemIssue")
                            +", AddnServices : "+((JSONObject)jsonArray.get(position)).getString("additionalService")
                    );

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return row;
            }
        };
        loadData();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additional_service);
    }
    private void loadData(){
        ListView listView = (ListView)findViewById(R.id.listView);
        listView.setAdapter(adapter);

        AsyncHttpClient client = new AsyncHttpClient(){
            @Override
            public void setTimeout(int value) {
                super.setTimeout(60 * 1000);
            }
        };
        String url = "http://earningindia.com/VMMS/getServices.php";

        SharedPreferences prefs = getSharedPreferences("preferences", MODE_PRIVATE);
        String idNumber = prefs.getString("id", "0");

        RequestParams params = new RequestParams();
        params.put("userId", idNumber);

        client.post(url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                progress.dismiss();
                System.out.println("response = "+response);
                final String tempRes = response.toString();
                try{
                    if (!tempRes.equalsIgnoreCase("No Such User Found")) {
                        for(int i = 0; i<response.length();i++){
                            jsonArray = response;
                            JSONObject obj = (JSONObject)response.get(i);
                            listItems.add(obj.getString("serviceName")+" ,"+obj.getString("serviceType"));
                        }
                        adapter.notifyDataSetChanged();

                    }
                }catch (JSONException e){

                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progress.dismiss();
                showAlert("Error", "No services found.");
            }
        });
    }
    public void showAlert(final String title, final String message) {
        MyServices.this.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(MyServices.this);
                builder.setTitle(title);
                builder.setCancelable(true);
                builder.setMessage(message);
                builder.setPositiveButton("Thank You",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            MyServices.this.runOnUiThread(new Runnable() {
                public void run() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MyServices.this);
                    builder.setTitle("Logout");
                    builder.setCancelable(true);
                    builder.setMessage("Are you sure you want to logout?");
                    builder.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            MyServices.this.finish();
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {}
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}