package com.vehiclemaintanancesyatem;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.telephony.PhoneNumberFormattingTextWatcher;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
@SuppressLint("NewApi")
public class UserRegistration extends Activity {

	JSONParser jsonParser = new JSONParser();
	String fullname, address, vrn, conno, uname, pass, answer,sqr,spin;
	EditText edfullname, edaddress, edvrn, edconno, eduname, edpass, edanswer;
	Spinner sq;
InputStream is=null;
	
	String result=null;
	String line=null;
	int code;
	Button submit;
    ProgressDialog progress;
    Context context;
	
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
		  .detectDiskReads().detectDiskWrites().detectNetwork() // StrictMode is most commonly used to catch accidental disk or network access on the application's main thread
		  .penaltyLog().build());
		setContentView(R.layout.registration);
        context = this;
		
		edfullname = (EditText) findViewById(R.id.edtregusr);
		edfullname.addTextChangedListener(new TextWatcher() 
		 		{
	            	public void afterTextChanged(Editable s)
	            	{
	            		Validation.hasText(edfullname);
	            	}
	            	public void beforeTextChanged(CharSequence s, int start, int count, int after)
	            	{
	            		
	            	}
	            	public void onTextChanged(CharSequence s, int start, int before, int count)
	            	{
	            		
	            	}
				
				
		 });
		 
		edaddress = (EditText) findViewById(R.id.edtregadd);
		edaddress.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) 
            {
                Validation.hasText(edaddress);
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            	
            }
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            	
            }
			
			
	    });
		
		edvrn = (EditText) findViewById(R.id.edtregvrn);
		edvrn.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s)
            {
                Validation.hasText(edvrn);
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            	
            }
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            	
            }
			
			
	    });
		
		edconno = (EditText) findViewById(R.id.edtregcon);
//		edconno.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
		
		eduname = (EditText) findViewById(R.id.edtregunm);
		eduname.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s)
            {
                Validation.hasText(eduname);
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            	
            }
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            	
            }
			
			
	    });
		
		edpass = (EditText) findViewById(R.id.edtregpass);
		edpass.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s)
            {
                Validation.isPassword(edpass, false);
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            	
            }
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            	
            }
        });
		
		edanswer = (EditText) findViewById(R.id.edtregsqans);
		edanswer.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s)
            {
                Validation.hasText(edanswer);
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            	
            }
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            	
            }
			
			
	    });
		
		sq = (Spinner) findViewById(R.id.spinner1);
		submit = (Button) findViewById(R.id.regsbt);
				
		
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
                  
				fullname=edfullname.getText().toString();
				address=edaddress.getText().toString();
				vrn=edvrn.getText().toString();
				conno=edconno.getText().toString();
				uname=eduname.getText().toString();
				pass=edpass.getText().toString();
				spin=sq.getSelectedItem().toString();
			   	answer=edanswer.getText().toString();

                if(fullname.isEmpty()){
                    showAlert("Validation Error", "Full Name is required.");
                }else if(address.isEmpty()){
                    showAlert("Validation Error", "Address is required.");
                }else if(vrn.isEmpty()){
                    showAlert("Validation Error", "Vehicle Registration Number is required.");
                }else if(conno.isEmpty()){
                    showAlert("Validation Error", "Contact Number is required.");
                }else if(uname.isEmpty()){
                    showAlert("Validation Error", "Username is required.");
                }else if(pass.isEmpty()){
                    showAlert("Validation Error", "Password is required.");
                }else if(spin.isEmpty()){
                    showAlert("Validation Error", "Sequiruty Question is required.");
                }else if(answer.isEmpty()){
                    showAlert("Validation Error", "Answer is required.");
                }else {
                    progress = ProgressDialog.show(context, "Please Wait","Loading...", true, true);
                    insert();
                }
			}
		});
	}
		public void insert()
	    {
            AsyncHttpClient client = new AsyncHttpClient(){
                @Override
                public void setTimeout(int value) {
                    super.setTimeout(60 * 1000);
                }
            };
            String url = "http://earningindia.com/VMMS/insert.php";

            RequestParams params = new RequestParams();
            params.put("name", fullname);
            params.put("add", address);
            params.put("vrn", vrn);
            params.put("conno", conno);
            params.put("user", uname);
            params.put("pass", pass);
            params.put("sq", spin);
            params.put("ans", answer);

            client.post(url, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    progress.dismiss();
                    try {
                        JSONObject obj = (JSONObject) response.get(0);
                        System.out.println(response.toString());
                        progress.dismiss();
                        Toast.makeText(getApplicationContext(), "Registration Successful. Please login.",
                                Toast.LENGTH_LONG).show();

                        UserRegistration.super.onBackPressed();
                    } catch (JSONException e) {
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    showAlert("Registration Failed","Some error occured. Registration is not successful.");
                    progress.dismiss();
                }
            });
        }
    public void showAlert(final String title, final String message) {
        UserRegistration.this.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(UserRegistration.this);
                builder.setTitle(title);
                builder.setCancelable(true);
                builder.setMessage(message);
                builder.setPositiveButton("Thank You",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
}
