package com.vehiclemaintanancesyatem;
 
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class EditProfile extends Activity {
    ProgressDialog progress;
    Context context;
    EditText fullName;
    EditText address;
    EditText contactNo;
    Button updateButton;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        context = this;

        fullName = (EditText) findViewById(R.id.edtregusr);
        address = (EditText) findViewById(R.id.edtregadd);
        contactNo = (EditText) findViewById(R.id.edtregcon);
        updateButton = (Button) findViewById(R.id.updatebt);

        SharedPreferences prefs = getSharedPreferences("preferences", MODE_PRIVATE);
        final String idNumber = prefs.getString("id", "0");
        String oldAddress = prefs.getString("address", "0");
        String oldContactNo = prefs.getString("contactno", "0");
        String oldFullName = prefs.getString("fullname", "0");
        fullName.setText(oldFullName);
        address.setText(oldAddress);
        contactNo.setText(oldContactNo);

        updateButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                String fName = fullName.getText().toString();
                String add = address.getText().toString();
                String con = contactNo.getText().toString();

                if(fName.isEmpty()){
                    showAlert("Validation Error", "Full Name is empty");
                }else if(add.isEmpty()){
                    showAlert("Validation Error", "Address is empty");
                }else if(con.isEmpty()){
                    showAlert("Validation Error", "Contact Number is empty");
                }else {
                    progress = ProgressDialog.show(context, "Please Wait","Loading...", true, true);
                    AsyncHttpClient client = new AsyncHttpClient(){
                        @Override
                        public void setTimeout(int value) {
                            super.setTimeout(60 * 1000);
                        }
                    };
                    String url = "http://earningindia.com/VMMS/updateProfile.php";

                    RequestParams params = new RequestParams();
                    params.put("id", idNumber);
                    params.put("fullname", fName);
                    params.put("address", add);
                    params.put("contactno", con);

                    client.post(url, params, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                            progress.dismiss();
                            System.out.println("response = "+response);
                            final String tempRes = response.toString();
                            try{
                                if (!tempRes.equalsIgnoreCase("No Such User Found")) {
                                    JSONObject obj = (JSONObject)response.get(0);
                                    String msg = "Profile Updated Successfully!";
                                    Toast.makeText(getApplicationContext(), (String) msg,
                                            Toast.LENGTH_LONG).show();
                                }
                            }catch (JSONException e){

                            }
                        }
                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            progress.dismiss();
                            showAlert("Error", "Login Failed. Invalid username or password.");
                        }
                    });
                }
            }
        });
    }
    public void showAlert(final String title, final String message) {
        EditProfile.this.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(EditProfile.this);
                builder.setTitle(title);
                builder.setCancelable(true);
                builder.setMessage(message);
                builder.setPositiveButton("Thank You",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            EditProfile.this.runOnUiThread(new Runnable() {
                public void run() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(EditProfile.this);
                    builder.setTitle("Logout");
                    builder.setCancelable(true);
                    builder.setMessage("Are you sure you want to logout?");
                    builder.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            EditProfile.this.finish();
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {}
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}