package com.vehiclemaintanancesyatem;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ForgetPassword  extends Activity {
    Context context;
    Spinner sq;
    Button fpsubmit;
    EditText fpusername, fpanswer;
    String username, answer, question;
    ProgressDialog progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgetpassword);
        context = this;
        fpusername = (EditText) findViewById(R.id.fpusername);
        sq = (Spinner) findViewById(R.id.fpquestion);
        fpanswer = (EditText) findViewById(R.id.fpanswer);
        fpsubmit = (Button) findViewById(R.id.fpbutton);

        fpsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                username = fpusername.getText().toString();
                question=sq.getSelectedItem().toString();
                answer = fpanswer.getText().toString();
                if(username.isEmpty()){
                    showAlert("Validation Error", "Username is empty");
                }else if(question.equals("---Select Your Question---")){
                    showAlert("Validation Error", "Please select your question.");
                }else if(answer.isEmpty()){
                    showAlert("Validation Error", "Please answer for your secret question.");
                }else {
                    progress = ProgressDialog.show(context, "Please Wait", "Loading...", true, true);
                    forgetPassword();
                }
            }
        });
    }

    private void forgetPassword() {
        AsyncHttpClient client = new AsyncHttpClient(){
            @Override
            public void setTimeout(int value) {
                super.setTimeout(60 * 1000);
            }
        };
        String url = "http://earningindia.com/VMMS/forget.php";

        RequestParams params = new RequestParams();
        params.put("username", username);
        params.put("question", question);
        params.put("answer", answer);
        System.out.println(params.toString());
        client.post(url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                progress.dismiss();
                System.out.println("response = "+response);
                final String tempRes = response.toString();
                try{
                    if (!tempRes.equalsIgnoreCase("No Such User Found")) {
                        JSONObject obj = (JSONObject)response.get(0);
                        String msg = "Your password is : "+obj.getString("password");
                        showAlert("Your password",msg);
                    }
                }catch (JSONException e){
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progress.dismiss();
                showAlert("Error", "Invalid username or question/answer.");
            }
        });
    }

    public void showAlert(final String title, final String message) {
        ForgetPassword.this.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPassword.this);
                builder.setTitle(title);
                builder.setCancelable(true);
                builder.setMessage(message);
                builder.setPositiveButton("Thank You",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
}
