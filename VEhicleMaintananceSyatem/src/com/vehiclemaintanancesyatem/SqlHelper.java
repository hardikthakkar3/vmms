package com.vehiclemaintanancesyatem;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class SqlHelper {

	public static final String FULLNAME = "fullname";
	public static final String ADDRESS = "address";
	public static final String VCN = "vcno";
	public static final String CONNO = "conno";
	public static final String USERNAME = "uname";
	public static final String PASSWORD = "pass";
	public static final String SECURITYQUE = "sq";
	public static final String SECURITYANS = "sqans";
	public static final String COLUMN_ID = "id";

	public static final String TABLE_NAME = "regestration";
	public static final String DATABASE_NAME = "vms.db";
	public static final int DATABASE_VERSION = 1;

	public static final String TABLE_CREATE = "create table regestration(id  integer primary key autoincrement, fullname text not null, address text not null, vcno text not null, conno text not null, uname text not null, pass text not null, sq text not null, sqans text not null )";

	DatabaseHelper dbHelper;
	Context ctx;
	SQLiteDatabase db;

	public SqlHelper(Context ctx) {
		this.ctx = ctx;
		dbHelper = new DatabaseHelper(ctx);

	}

	public static class DatabaseHelper extends SQLiteOpenHelper

	{

		public DatabaseHelper(Context ctx) {
			super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			try {
				db.execSQL(TABLE_CREATE);

			} catch (SQLException e)

			{
				e.printStackTrace();
			}

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			db.execSQL("DROP TABLE IF EXISTS regestration");

			onCreate(db);

		}

	}

	public SqlHelper open() {
		db = dbHelper.getWritableDatabase();
		return this;

	}

	public void close() {
		dbHelper.close();
	}

	public long insertdata(Contacts contact) {

		db = dbHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(FULLNAME, contact.getName());
		values.put(ADDRESS, contact.getAddress());
		values.put(VCN, contact.getVcno());
		values.put(CONNO, contact.getConno());
		values.put(USERNAME, contact.getUname());
		values.put(PASSWORD, contact.getPassword());
		values.put(SECURITYQUE, contact.getSeque());
		values.put(SECURITYANS, contact.getSeque());

		return db.insertOrThrow(TABLE_NAME, null, values);

	}

}
