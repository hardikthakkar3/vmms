package com.vehiclemaintanancesyatem;
 
import android.app.*;
import android.content.*;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.*;
import com.google.android.gcm.GCMBaseIntentService;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class Services extends Activity {
    ProgressDialog progress;
    Context context;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);
        context = this;
        Button submitButton = (Button)findViewById(R.id.sersbt);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String serviceName = ((EditText)findViewById(R.id.edtserusr)).getText().toString();
                String problemIssue = ((EditText)findViewById(R.id.edtserpro)).getText().toString();
                String additionalServiceDetails = ((EditText)findViewById(R.id.edtseraddser)).getText().toString();
                String partsTobeReplaced = ((EditText)findViewById(R.id.edtserpart)).getText().toString();
                String serviceType = ((RadioButton)findViewById(((RadioGroup)findViewById(R.id.radioService)).getCheckedRadioButtonId())).getText().toString();
                if(serviceName.isEmpty()){
                    showAlert("Validation Error","Service Name is required");
                }else {
                    progress = ProgressDialog.show(context, "Please Wait","Loading...", true, true);
                    AsyncHttpClient client = new AsyncHttpClient(){
                        @Override
                        public void setTimeout(int value) {
                            super.setTimeout(60 * 1000);
                        }
                    };
                    String url = "http://earningindia.com/VMMS/createService.php";
                    SharedPreferences prefs = getSharedPreferences("preferences", MODE_PRIVATE);
                    String idNumber = prefs.getString("id", "0");

                    RequestParams params = new RequestParams();
                    params.put("userId", idNumber);
                    params.put("serviceName", serviceName);
                    params.put("additionalServiceName", additionalServiceDetails);
                    params.put("replaceParts", partsTobeReplaced);
                    params.put("problemIssue", problemIssue);
                    params.put("serviceType", serviceType);
                    System.out.println("Request Params = "+params.toString());




                    client.post(url, params, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                            progress.dismiss();
                            System.out.println("response = "+response);
                            final String tempRes = response.toString();
                            if (!tempRes.equalsIgnoreCase("No Service Request Found")) {
                                String msg = "Service Request Submitted Successfully!";
                                Toast.makeText(getApplicationContext(), (String) msg,
                                        Toast.LENGTH_LONG).show();

                                NotificationManager notificationManager = (NotificationManager)
                                        getSystemService(NOTIFICATION_SERVICE);
                                Intent intent = new Intent(context, MyServices.class);
                                PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);
                                Notification n  = new Notification.Builder(context)
                                        .setContentTitle("Service Request Received")
                                        .setContentText("We have received new service request from you.")
                                        .setSmallIcon(R.drawable.ic_launcher)
                                        .setContentIntent(pIntent)
                                        .setAutoCancel(true).build();
                                notificationManager.notify(0, n);

                            }
                        }
                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            progress.dismiss();
                            System.out.println("response = "+responseString);
                            showAlert("Error", "Unable to create service. Some error occured.");
                        }
                    });
                }
            }
        });
    }
    public void showAlert(final String title, final String message) {
        Services.this.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(Services.this);
                builder.setTitle(title);
                builder.setCancelable(true);
                builder.setMessage(message);
                builder.setPositiveButton("Thank You",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Services.this.runOnUiThread(new Runnable() {
                public void run() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Services.this);
                    builder.setTitle("Logout");
                    builder.setCancelable(true);
                    builder.setMessage("Are you sure you want to logout?");
                    builder.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            Services.this.finish();
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {}
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
