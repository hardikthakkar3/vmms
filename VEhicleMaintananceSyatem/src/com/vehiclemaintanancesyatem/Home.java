package com.vehiclemaintanancesyatem;

import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
 
public class Home extends TabActivity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
         
        TabHost tabHost = getTabHost();
         
        // Tab for services
        TabSpec servicespec = tabHost.newTabSpec("Services");
        // setting Title and Icon for the Tab
        servicespec.setIndicator("Services", getResources().getDrawable(R.drawable.services));
        Intent serviceIntent = new Intent(this, Services.class);
        servicespec.setContent(serviceIntent);
         
        // Tab for additional service
        TabSpec addservicespec = tabHost.newTabSpec("My Services");
        addservicespec.setIndicator("My Services", getResources().getDrawable(R.drawable.additionalservice));
        Intent addserviceIntent = new Intent(this, MyServices.class);
        addservicespec.setContent(addserviceIntent);
        
        // Tab for edit profile
        TabSpec editprofilespec = tabHost.newTabSpec("Edit Profile");
        editprofilespec.setIndicator("Edit Profile", getResources().getDrawable(R.drawable.editprofile));
        Intent edtprofileIntent = new Intent(this, EditProfile.class);
        editprofilespec.setContent(edtprofileIntent);
        
        // Tab for contact us
        TabSpec contactusspec = tabHost.newTabSpec("Contact Us");
        contactusspec.setIndicator("Contact Us", getResources().getDrawable(R.drawable.contactus));
        Intent contactIntent = new Intent(this, ContactUs.class);
        contactusspec.setContent(contactIntent);
         
        // Adding all TabSpec to TabHost
        tabHost.addTab(servicespec); // Adding service tab
        tabHost.addTab(addservicespec); // Adding additional service tab
        tabHost.addTab(editprofilespec); // Adding edit profile tab
        tabHost.addTab(contactusspec);// Adding contact us tab
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                //do stuff here
                showAlert("Logout", "Are you sure you want to logout?");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void showAlert(final String title, final String message) {
        Home.this.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(Home.this);
                builder.setTitle(title);
                builder.setCancelable(true);
                builder.setMessage(message);
                builder.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        Home.this.finish();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

}